﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt1_Patryk_Bryja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja.Tests
{
    [TestClass()]
    public class VisibilityTests
    {
        [TestMethod()]
        public void GenerateTest()
        {
            const int size = 10;

            string[,] correct = new string[size, size] {
                { "#", "#","#","#","#","#","#","#","#","#" },
                { "#", " "," "," "," "," "," "," "," ","#" },
                { "#", " ","#","#","#","#","#","#"," ","#" },
                { "#", " ","#"," "," "," "," ","#"," ","#" },
                { "#", " ","#"," ","#","#"," ","#"," ","#" },
                { "#", " ","#"," ","#","#"," ","#"," ","#" },
                { "#", " ","#"," "," "," "," ","#"," ","#" },
                { "#", " ","#","#","#","#","#","#"," ","#" },
                { "#", " "," "," "," "," "," "," "," ","#" },
                { "#", "#","#","#","#","#","#","#","#","#" },
            };
            EasyMazeBuilder generator = new EasyMazeBuilder();
            generator.CreateMaze(size);
            string[,] board = new string[size, size];
            Maze maze = generator.Maze;
            FieldPrototype[,] mazeBoard = maze.Board;
            generator.GenerateDefaultFields();
            Visibility vis = new Visibility();

            Console.WriteLine(vis.Generate(new Point(1,1), maze));

        }
    }
}