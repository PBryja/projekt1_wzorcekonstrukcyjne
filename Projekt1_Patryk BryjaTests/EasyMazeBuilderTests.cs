﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt1_Patryk_Bryja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja.Tests
{
    [TestClass()]
    public class EasyMazeBuilderTests
    {
        [TestMethod()]
        public void CreateAddRandomRoomsTest_()
        {
            //AAA
            Circuit circuit = new Circuit(2, 7);
            EasyMazeBuilder builder = new EasyMazeBuilder();

            builder.CreateMaze(10);

            builder.GenerateDefaultFields();
            builder.CreateAdditionalRandomRooms(circuit);

            Maze maze = builder.Maze;

            int count = 0;

            foreach (var item in circuit.GetAllPointsWithoutCorners())
            {
                if (maze.Board[item.X, item.Y].GetType() == typeof(Room))
                    count++;
            }

            Assert.AreEqual(4, count);

            //for (int x = 0; x < maze.Size; x++)
            //{
            //    for (int y = 0; y < maze.Size; y++)
            //    {
            //        if (maze.Board[x,y].GetType() == typeof(Room))
            //            Console.Write(" ");
            //        else
            //            Console.Write("#");
            //    }
            //    Console.WriteLine();
            //}
        }

        [TestMethod()]
        public void CreateRandomRoomsInDefaultWallsTest()
        {
            SupervisorMaze supervisor = new SupervisorMaze();
            EasyMazeBuilder builder = new EasyMazeBuilder();
            Maze maze = supervisor.ConstructMaze(builder);

            //for (int x = 0; x < maze.Size; x++)
            //{
            //    for (int y = 0; y < maze.Size; y++)
            //    {
            //        if (maze.Board[x, y].GetType() == typeof(Room))
            //            Console.Write("O");
            //        else
            //            Console.Write("#");
            //    }
            //    Console.WriteLine();
            //}

            int howMany = (int)(maze.Size / 2);
            Circuit circuit = new Circuit(0, 0);
            int count = 0;

            for (int i = 1; i < howMany; i++)
            {
                count = 0;
                circuit.SetCoordinates(i, maze.Size - i - 1);
                List<Point> list = circuit.GetAllPointsWithoutCorners();
                
                if (i % 2 == 0)
                {
                    foreach (var item in list)
                    {

                        if (maze.Board[item.X, item.Y].GetType() == typeof(Room))
                            count++;
                    }

                    if (count < 4)
                        Assert.Fail("" + i);
                }     
            }
        }
    }
}