﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt1_Patryk_Bryja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja.Tests
{
    [TestClass()]
    public class CircuitTests
    {
        private int end = 11;
        private int start = 0;

        [TestMethod()]
        public void GetVerticalLineFromEndWithoutCorners_CorrectCoordinates_ReturnExpectedValues()
        {
            //AAA

            for (int i = start; i <= end; i++)
            {
                int start = i;
                int end = this.end - i;
                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {end+","+(start+1)},
                {end+","+(start+2)},
                {end+","+(start+3)},
                {end+","+(start+4)},
                {end+","+(start+5)},
                {end+","+(start+6)},
                {end+","+(start+7)},
                {end+","+(start+8)},
                {end+","+(start+9)},
                {end+","+(start+10)}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start - 1, expectedList.Count + start - (end - 1));


                //AAA
                list = circuit.GetVerticalLineFromEndWithoutCorners();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }


                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }

            //AAA
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetVerticalLineFromStartWithoutCorners_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {start+","+(start+1)},
                {start+","+(start+2)},
                {start+","+(start+3)},
                {start+","+(start+4)},
                {start+","+(start+5)},
                {start+","+(start+6)},
                {start+","+(start+7)},
                {start+","+(start+8)},
                {start+","+(start+9)},
                {start+","+(start+10)}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start - 1, expectedList.Count + start - (end - 1));

                //AAA
                list = circuit.GetVerticalLineFromStartWithoutCorners();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }

                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }
            //AAA
            Assert.IsTrue(true);

        }

        [TestMethod()]
        public void GetHorizontalLineFromEndWithoutCorners_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {(start+1)+","+end},
                {(start+2)+","+end},
                {(start+3)+","+end},
                {(start+4)+","+end},
                {(start+5)+","+end},
                {(start+6)+","+end},
                {(start+7)+","+end},
                {(start+8)+","+end},
                {(start+9)+","+end},
                {(start+10)+","+end}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start - 1, expectedList.Count + start - (end - 1));

                //AAA
                list = circuit.GetHorizontalLineFromEndWithoutCorners();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }

                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }
            //AAA
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetHorizontalLineFromStartWithoutCorners_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {(start+1)+","+start},
                {(start+2)+","+start},
                {(start+3)+","+start},
                {(start+4)+","+start},
                {(start+5)+","+start},
                {(start+6)+","+start},
                {(start+7)+","+start},
                {(start+8)+","+start},
                {(start+9)+","+start},
                {(start+10)+","+start}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start - 1, expectedList.Count + start - (end - 1));

                //AAA
                list = circuit.GetHorizontalLineFromStartWithoutCorners();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }

                //AAA
                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetVerticalLineFromEnd_CorrectCoordinates_ReturnExpectedValues()
        {

            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;
                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {end+","+start},
                {end+","+(start+1)},
                {end+","+(start+2)},
                {end+","+(start+3)},
                {end+","+(start+4)},
                {end+","+(start+5)},
                {end+","+(start+6)},
                {end+","+(start+7)},
                {end+","+(start+8)},
                {end+","+(start+9)},
                {end+","+(start+10)},
                {end+","+end}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start + 1, expectedList.Count + start - (end + 1));



                //AAA
                list = circuit.GetVerticalLineFromEnd();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }
                //AAA
                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetVerticalLineFromStart_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {start+","+start},
                {start+","+(start+1)},
                {start+","+(start+2)},
                {start+","+(start+3)},
                {start+","+(start+4)},
                {start+","+(start+5)},
                {start+","+(start+6)},
                {start+","+(start+7)},
                {start+","+(start+8)},
                {start+","+(start+9)},
                {start+","+(start+10)},
                {start+","+end}
            };
                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start + 1, expectedList.Count + start - (end + 1));

                //AAA
                list = circuit.GetVerticalLineFromStart();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }
                //AAA
                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetHorizontalLineFromEnd_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {start+","+end },
                {(start+1)+","+end},
                {(start+2)+","+end},
                {(start+3)+","+end},
                {(start+4)+","+end},
                {(start+5)+","+end},
                {(start+6)+","+end},
                {(start+7)+","+end},
                {(start+8)+","+end},
                {(start+9)+","+end},
                {(start+10)+","+end},
                {end+","+end },
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start + 1, expectedList.Count + start - (end + 1));
                //AAA
                list = circuit.GetHorizontalLineFromEnd();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }
                //AAA
                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetHorizontalLineFromStart_CorrectCoordinates_ReturnExpectedValues()
        {
            for (int i = start; i <= end; i++)
            {
                //AAA
                int start = i;
                int end = this.end - i;

                Circuit circuit = new Circuit(start, end);
                List<Point> list = new List<Point>();
                List<string> listCoordinates = new List<string>();
                List<string> expectedList = new List<string>
            {
                {start+","+start},
                {(start+1)+","+start},
                {(start+2)+","+start},
                {(start+3)+","+start},
                {(start+4)+","+start},
                {(start+5)+","+start},
                {(start+6)+","+start},
                {(start+7)+","+start},
                {(start+8)+","+start},
                {(start+9)+","+start},
                {(start+10)+","+start},
                {end+","+start}
            };

                if (end - start <= 0)
                    expectedList = new List<string>();
                else
                    expectedList.RemoveRange(end - start + 1, expectedList.Count + start - (end + 1));

                //AAA
                list = circuit.GetHorizontalLineFromStart();

                foreach (var item in list)
                {
                    listCoordinates.Add(item.X + "," + item.Y);
                }
                //AAA

                if (!listCoordinates.SequenceEqual(expectedList))
                    Assert.Fail("i: " + i);
            }

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void GetAllPointsTest_CorrectCoordinates_ReturnExpectedValues()
        {
            //AAA
            Circuit circuit = new Circuit(0, 3);
            List<Point> list = new List<Point>();
            List<string> listCoordinates = new List<string>();
            List<string> expectedList = new List<string>
            {
                {"0,0"},
                {"0,1"},
                {"0,2"},
                {"0,3"},
                {"1,0"},
                {"1,3"},
                {"2,0"},
                {"2,3"},
                {"3,0"},
                {"3,1"},
                {"3,2"},
                {"3,3"}
            };

            //AAA
            list = circuit.GetAllPoints();

            foreach (var item in list)
            {
                listCoordinates.Add(item.X + "," + item.Y);
            }

            //AAA
            CollectionAssert.AreEqual(expectedList, listCoordinates);
        }

        [TestMethod()]
        public void GetOneRandomFieldInCircuitWithoutCornersTest_SetCorrectProperties_ReturnOneOfExpectedValues()
        {
            //AAA
            for (int i = start; i < end; i++)
            {
                int start = i;
                int end = this.end - i;

                if (end - start <= 1)
                    continue;

                Circuit circuit = new Circuit(start, end);
                List<string> listPoint = new List<string>();

                List<Point> points = circuit.GetAllPointsWithoutCorners();

                foreach (var item in points)
                {
                    listPoint.Add(item.ToString());
                }

                string rndPoint = circuit.GetOneRandomFieldInCircuitWithoutCorners().ToString();

                if (i == 4)
                    if (!listPoint.Contains(rndPoint))
                        Assert.Fail("" + i);
            }


            Assert.IsTrue(true);
        }
    }
}