﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt1_Patryk_Bryja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja.Tests
{
    [TestClass()]
    public class MovementTests
    {
        EasyMazeBuilder builder;
        SupervisorMaze supervisor;
        Maze maze;
        Movement movement;

        public void Prepare()
        {
            builder = new EasyMazeBuilder();
            supervisor = new SupervisorMaze();
            maze = supervisor.ConstructMaze(builder);
            movement = new Movement(maze);
        }

        [TestMethod()]
        public void MoveTest_MoveUp_ReturnTrue()
        {
            //AAA
            Prepare();

            Point point = new Point(1, 1);
            Player player = new Player();
            player.SetPosition(point);
            Point expectedPoint = new Point(1, 2);

            //AAA
            Point result = movement.Move(player, "uparrow");

            //AAA
            Assert.AreEqual(expectedPoint.ToString(), result.ToString());
        }

        [TestMethod()]
        public void MoveTest_MoveUp_ReturnFalse()
        {
            //AAA
            Prepare();
            Point point = new Point(2, 1);
            Point expectedPoint = null;
            Player player = new Player();
            player.SetPosition(point);
            //AAA
            Point result = movement.Move(player, "uparrow");

            //AAA
            Assert.AreEqual(expectedPoint, result);

        }

        [TestMethod()]
        public void MoveTest_MoveDown_ReturnTrue()
        {
            //AAA
            Prepare();

            Point point = new Point(1, 2);
            Point expectedPoint = new Point(1, 1);
            Player player = new Player();
            player.SetPosition(point);

            //AAA
            Point result = movement.Move(player, "downarrow");

            //AAA
            Assert.AreEqual(expectedPoint.ToString(), result.ToString());

        }

        [TestMethod()]
        public void MoveTest_MoveDown_ReturnFalse()
        {
            //AAA
            Prepare();
            Point point = new Point(1, 1);
            Point expectedPoint = null;
            Player player = new Player();
            player.SetPosition(point);
            //AAA
            Point result = movement.Move(player, "downarrow");

            //AAA
            Assert.AreEqual(expectedPoint, result);

        }

        [TestMethod()]
        public void MoveTest_MoveLeft_ReturnTrue()
        {
            //AAA
            Prepare();
            Point point = new Point(2, 1);
            Point expectedPoint = new Point(1, 1);
            Player player = new Player();
            player.SetPosition(point);

            //AAA
            Point result = movement.Move(player, "leftarrow");

            //AAA
            Assert.AreEqual(expectedPoint.ToString(), result.ToString());

        }

        [TestMethod()]
        public void MoveTest_MoveLeft_ReturnFalse()
        {
            //AAA
            Prepare();
            Point point = new Point(1, 1);
            Point expectedPoint = null;
            Player player = new Player();
            player.SetPosition(point);

            //AAA
            Point result = movement.Move(player, "leftarrow");

            //AAA
            Assert.AreEqual(expectedPoint, result);

        }

        [TestMethod()]
        public void MoveTest_MoveRight_ReturnTrue()
        {
            //AAA
            Prepare();
            Point point = new Point(1, 1);
            Point expectedPoint = new Point(2, 1);
            Player player = new Player();
            player.SetPosition(point);

            //AAA
            Point result = movement.Move(player, "rightarrow");

            //AAA
            Assert.AreEqual(expectedPoint.ToString(), result.ToString());
        }

        [TestMethod()]
        public void MoveTest_MoveRight_ReturnFalse()
        {
            //AAA
            Prepare();
            Point point = new Point(1, 2);
            Point expectedPoint = null;
            Player player = new Player();
            player.SetPosition(point);

            //AAA
            Point result = movement.Move(player, "rightarrow");

            //AAA
            Assert.AreEqual(expectedPoint, result);

        }
    }
}