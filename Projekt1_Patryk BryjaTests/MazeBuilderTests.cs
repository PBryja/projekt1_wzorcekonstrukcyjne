﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Projekt1_Patryk_Bryja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja.Tests
{
    [TestClass()]
    public class MazeBuilderTests
    {
        [TestMethod()]
        public void GenerateDefaultFieldsTest_CreateMazeAndCompare_ReturnSameMazeAsExpected()
        {
            const int size = 10;

            string[,] correct = new string[size, size] {
                { "#", "#","#","#","#","#","#","#","#","#" },
                { "#", " "," "," "," "," "," "," "," ","#" },
                { "#", " ","#","#","#","#","#","#"," ","#" },
                { "#", " ","#"," "," "," "," ","#"," ","#" },
                { "#", " ","#"," ","#","#"," ","#"," ","#" },
                { "#", " ","#"," ","#","#"," ","#"," ","#" },
                { "#", " ","#"," "," "," "," ","#"," ","#" },
                { "#", " ","#","#","#","#","#","#"," ","#" },
                { "#", " "," "," "," "," "," "," "," ","#" },
                { "#", "#","#","#","#","#","#","#","#","#" },
            };


            EasyMazeBuilder generator = new EasyMazeBuilder();
            generator.CreateMaze(size);
            string[,] board = new string[size, size];
            Maze maze = generator.Maze;
            FieldPrototype[,] mazeBoard = maze.Board;

            //AAA
            generator.GenerateDefaultFields();

            for (int i = 0; i < size; i++)
            {
                for (int y = 0; y < size; y++)
                {
                    if (mazeBoard[i, y].GetType().Equals(typeof(Room)))
                        board[i, y] = " ";
                    else if (mazeBoard[i, y].GetType().Equals(typeof(Wall)))
                        board[i, y] = "#";
                    else board[i, y] = "X";
                }
            }

            //Comparing Single Chars
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if (!board[x, y].Equals(correct[x, y]))
                        Assert.Fail();
                }
            }

            //AAA
            CollectionAssert.AreEqual(correct, board);
        }
    }
}