﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    abstract class MazeBuilder
    {
        protected Random rand;
        protected Maze maze;
        protected Room room = new Room();
        protected Wall wall = new Wall();
        protected int size;
        protected delegate FieldPrototype DCreate();

        public  void SetEndPoint()
        {
            bool finish = false;

            while (!finish)
            {
                FieldPrototype[,] board = maze.Board;
                int x = rand.Next(1, maze.Size);
                int y = rand.Next(1, maze.Size);

                if (!(board[x, y].GetType() == typeof(Wall)) && (x != 1 && y != 1))
                {
                    finish = true;
                    maze.SetField(new Point(x, y), new RoomEnd());
                }
            }
        }

        public void SetKey()
        {
            bool finish = false;

            while (!finish)
            {
                FieldPrototype[,] board = maze.Board;
                int x = rand.Next(1, maze.Size);
                int y = rand.Next(1, maze.Size);

                if (!(board[x, y].GetType() == typeof(Wall)) && (x != 1 && y != 1) && !(board[x, y] is RoomEnd))
                {
                    finish = true;
                    maze.SetField(new Point(x, y), new RoomKey());
                }
            }
        }

        public void SetDoor()
        {
            bool finish = false;

            while (!finish)
            {
                FieldPrototype[,] board = maze.Board;
                int x = rand.Next(1, maze.Size);
                int y = rand.Next(1, maze.Size);

                if (!(board[x, y].GetType() == typeof(Wall)) && (x != 1 && y != 1) && !(board[x, y] is RoomEnd))
                {
                    finish = true;
                    maze.SetField(new Point(x, y), new WallDoor());
                }
            }
        }

        public MazeBuilder()
        {
            rand = new Random();
        }
        public Maze Maze { get => maze; }

        public void CreateMaze(int size)
        {
            maze = new Maze(size);
            this.size = size;
        }

        public void CreateMaze()
        {
            maze = new Maze(size);
        }

        protected Wall CreateWall()
        {
            Wall newWall = (Wall)wall.Clone();

            return newWall;
        }

        protected Room CreateRoom()
        {
            return (Room)room.Clone();
        }

        protected bool IsOnCircuit(int x, int y, int start, int end)
        {
            if (((x == start || x == end) && (y >= start && y <= end)) || ((y == start || y == end) && (x >= start && x <= end)))
                return true;
            else
                return false;
        }

        protected bool IsOnCorner(int x, int y, int start, int end)
        {
            if ((x == start || x == end) && (y == start || y == end))
                return true;
            else
                return false;
        }

        protected bool CreateCircuitFields(Circuit circuit, DCreate dCreate)
        {
            for (int x = circuit.Start; x <= circuit.End; x++)
            {
                for (int y = circuit.Start; y <= circuit.End; y++)
                {

                    if (IsOnCircuit(x, y, circuit.Start, circuit.End))
                        if (!maze.SetField(x, y, dCreate()))
                            return false;
                }
            }
            return true;
        }

        public virtual bool GenerateDefaultFields()
        {
            int howMany = (int)Math.Ceiling((Convert.ToDouble(size) / 2));
            DCreate createWall = CreateWall;
            DCreate createRoom = CreateRoom;
            Circuit circuit = new Circuit(0, 0);

            for (int i = 0; i < howMany; i++)
            {
                circuit.SetCoordinates(i, size - i - 1);
                if (i % 2 == 0)
                {
                    if (!CreateCircuitFields(circuit, createWall))
                        return false;
                }
                else
                {
                    if (!CreateCircuitFields(circuit, createRoom))
                        return false;
                }
            }

            return true;
        }

        public abstract bool CreateAdditionalRandomRooms(Circuit circuit);

        public bool CreateRandomRoomsInDefaultWalls()
        {
            int howMany = (int)Math.Ceiling((Convert.ToDouble(size) / 2));
            Circuit circuit = new Circuit(0, 0);

            for (int i = 1; i < howMany; i++)
            {
                circuit.SetCoordinates(i, size - i - 1);
                if (i % 2 == 0)
                {
                    if (!CreateAdditionalRandomRooms(circuit))
                        return false;
                }
            }
            return true;
        }
    }
}
