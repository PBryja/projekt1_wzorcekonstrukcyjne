﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Movement
    {
        public readonly Dictionary<string, int> moves = new Dictionary<string, int> { { "uparrow", 1 }, { "downarrow", -1 }, { "leftarrow", -1 }, { "rightarrow", 1 } };
        private Maze maze;

        public Movement(Maze maze)
        {
            this.maze = maze;
        }

        public Point Move(Player player, string direction)
        {
            Point point = player.CurrentPosition;
            direction = direction.ToLower();
            Point newPoint = (Point) point.Clone();

            if (direction.Equals("leftarrow") || direction.Equals("rightarrow"))
                newPoint.SetCoordinates(point.X + moves[direction], point.Y);
            else if (direction.Equals("downarrow") || direction.Equals("uparrow"))
                newPoint.SetCoordinates(point.X, point.Y + moves[direction]);
            else
                return null;

            if (IsAvailableToMove(newPoint, player))
                return newPoint;
            else
                return null;
        }

        protected bool IsAvailableToMove(Point point, Player player)
        {
            if (maze.Board[point.X, point.Y] is Room || (maze.Board[point.X, point.Y] is WallDoor && player.Key))
                return true;
            else
                return false;
        }
    }
}
