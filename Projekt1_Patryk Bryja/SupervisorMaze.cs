﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class SupervisorMaze
    {
        public Maze ConstructMaze(MazeBuilder builder)
        {
            builder.CreateMaze();
            builder.GenerateDefaultFields();
            builder.CreateRandomRoomsInDefaultWalls();
            builder.SetEndPoint();
            builder.SetDoor();
            builder.SetKey();

            return builder.Maze;
        }
    }
}
