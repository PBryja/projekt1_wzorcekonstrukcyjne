﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class GodVisibility : IVisibility
    {
        public string Generate(Point point, Maze maze)
        {
            string result = "";
            int x = point.X;
            int y = point.Y;
            FieldPrototype[,] board = maze.Board;

            for (int b = maze.Size-1; b >=0; b--)
            {
                for (int a = 0; a < maze.Size; a++)
                {
                    if (a == x && b == y)
                        result += "0";
                    else if (board[a, b] is Room)
                    {
                        if (board[a, b].GetType() == typeof(RoomKey))
                            result += "*";
                        else if (board[a, b].GetType() == typeof(RoomEnd))
                        {

                            result += "E";
                        }
                        else
                            result += " ";
                    }
                    else
                    {
                        if (board[a, b].GetType() == typeof(WallDoor))
                            result += "+";
                        else
                            result += "#";
                    }
                }
                result += "\n";
            }

            return result;

        }
    }
}
