﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Visibility : IVisibility
    {
        public string Generate(Point point, Maze maze)
        {
            string result = "";
            int x = point.X;
            int y = point.Y;
            FieldPrototype[,] board = maze.Board;

            FieldPrototype[,] fieldVis = new FieldPrototype[,]
            {
                { board[x-1,y+1], board[x,y+1], board[x+1,y+1]},
                { board[x-1,y], board[x,y], board[x+1,y]},
                { board[x-1,y-1], board[x,y-1], board[x+1,y-1]}
            };


            for (int a = 0; a < 3; a++)
            {
                for (int b = 0; b < 3; b++)
                {
                    if (a == 1 && b == 1)
                        result += "0";
                    else if (fieldVis[a, b] is Room)
                    {
                        if (fieldVis[a, b].GetType() == typeof(RoomKey))
                            result += "*";
                        else if (fieldVis[a, b].GetType() == typeof(RoomEnd))
                        {

                            result += "E";
                        }
                        else
                            result += " ";
                    }
                    else
                    {
                        if (fieldVis[a, b].GetType() == typeof(WallDoor))
                            result += "+";
                        else
                            result += "#";
                    }
                }
                result += "\n";
            }

            return result;

        }
    }
}
