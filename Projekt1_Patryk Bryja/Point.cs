﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Point : ICloneable
    {
        private int x;
        private int y;

        public Point(int x, int y)
        {
            SetCoordinates(x, y);
        }

        public void SetCoordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        public override string ToString()
        {
            return x+";"+y;
        }

        public bool Contains()
        {
            return false;
        }
    }
}
