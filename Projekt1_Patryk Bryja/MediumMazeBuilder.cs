﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class MediumMazeBuilder : MazeBuilder
    {
        private const int maxAddRooms = 3;

        public MediumMazeBuilder()
        {
            size = 50;
        }

        public override bool CreateAdditionalRandomRooms(Circuit circuit)
        {

            int countWalls = circuit.GetAllPointsWithoutCorners().Count;

            if (countWalls <= 3)
                return false;

            if (countWalls < 9)
            {
                Point chosen = circuit.GetOneRandomFieldInCircuitWithoutCorners();
                return maze.SetField(chosen, CreateRoom());
            }

            bool final = false;
            int count = 0;

            while (!final)
            {
                Point chosen = circuit.GetOneRandomFieldInCircuitWithoutCorners();

                if (maze.Board[chosen.X, chosen.Y].GetType() == typeof(Room))
                    continue;

                if ((circuit.End == chosen.X || circuit.Start == chosen.X) && (maze.Board[chosen.X, chosen.Y + 1].GetType() == typeof(Room) || maze.Board[chosen.X, chosen.Y - 1].GetType() == typeof(Room)))
                    continue;

                if ((circuit.End == chosen.Y || circuit.Start == chosen.Y) && (maze.Board[chosen.X + 1, chosen.Y].GetType() == typeof(Room) || maze.Board[chosen.X - 1, chosen.Y].GetType() == typeof(Room)))
                    continue;

                if (maze.SetField(chosen, CreateRoom()))
                    count++;

                if (countWalls > 15)
                {
                    if (count >= countWalls / maxAddRooms)
                        final = true;
                }
                else if (count == maxAddRooms)
                    final = true;
            }

            return true;
        }
    }
}
