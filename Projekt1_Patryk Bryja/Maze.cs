﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Maze
    {
        private FieldPrototype[,] board;
        private int size;

        public Maze(int size)
        {
            this.size = size;
            board = new FieldPrototype[size, size];
        }

        public bool SetField(int x, int y, FieldPrototype field)
        {
            if (x < 0 || x > size - 1 || y < 0 || y > size - 1)
                return false;

            board[x, y] = field;

            return true;
        }

        public bool SetField(Point point, FieldPrototype field)
        {
            return SetField(point.X, point.Y, field);
        }

        public int Size { get => size; }
        public FieldPrototype[,] Board { get => board; }
    }
}
