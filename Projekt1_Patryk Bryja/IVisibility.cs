﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    interface IVisibility
    {
        string Generate(Point point, Maze maze);
    }
}
