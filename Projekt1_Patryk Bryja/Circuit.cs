﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Circuit
    { 
        private int start;
        private int end;
        private Point point;
        private Random rand; 
        protected delegate int DGetPoint(Point point);

        public Circuit(int start, int end)
        {
            SetCoordinates(start, end);
            point = new Point(start, end);
            rand = new Random();
        }

        public Point GetOneRandomFieldInCircuitWithoutCorners()
        {
            List<Point> points = GetAllPointsWithoutCorners();
            if (points.Count == 0)
                return null;

            Point chosen = points[rand.Next(0, points.Count)];

            return chosen;
        }

        public void SetCoordinates(int start, int end)
        {
            Start = start;
            End = end;
        }

        private int GetFromPointY(Point point)
        {
            return point.Y;
        }

        private int GetFromPointX(Point point)
        {
            return point.X;
        }

        private List<Point> GetLine(DGetPoint dGetPoint, int pointToCompare)
        {
            List<Point> list = GetAllPoints();
            List<Point> resultList = new List<Point>();

            foreach (var item in list)
            {
                if (dGetPoint(item) == pointToCompare)
                {
                    Point correctPoint = (Point)item.Clone();
                    correctPoint.SetCoordinates(item.X, item.Y);

                    resultList.Add(correctPoint);
                }
            }
            return resultList;
        }

        private List<Point> GetLineWithoutCorners(DGetPoint dGetPoint, int pointToCompare)
        {
            List<Point> list = GetAllPointsWithoutCorners();
            List<Point> resultList = new List<Point>();

            foreach (var item in list)
            {
                if (dGetPoint(item) == pointToCompare)
                {
                    Point correctPoint = (Point)item.Clone();
                    correctPoint.SetCoordinates(item.X, item.Y);

                    resultList.Add(correctPoint);
                }
            }
            return resultList;
        }

        public List<Point> GetVerticalLineFromEnd()
        {
            return GetLine(GetFromPointX, end);
        }

        public List<Point> GetVerticalLineFromStart()
        {

            return GetLine(GetFromPointX, start);
        }

        public List<Point> GetHorizontalLineFromEnd()
        {
            return GetLine(GetFromPointY, end);
        }

        public List<Point> GetHorizontalLineFromStart()
        {
            return GetLine(GetFromPointY,start);
        }

        public List<Point> GetHorizontalLineFromStartWithoutCorners()
        {
            return GetLineWithoutCorners(GetFromPointY, start);
        }

        public List<Point> GetHorizontalLineFromEndWithoutCorners()
        {
            return GetLineWithoutCorners(GetFromPointY, end);
        }

        public List<Point> GetVerticalLineFromEndWithoutCorners()
        {
            return GetLineWithoutCorners(GetFromPointX, end);
        }

        public List<Point> GetVerticalLineFromStartWithoutCorners()
        {

            return GetLineWithoutCorners(GetFromPointX, start);
        }

        public List<Point> GetAllPoints()
        {
            List<Point> list = new List<Point>();

            for (int x = start; x <= end; x++)
            {
                for (int y = start; y <= end; y++)
                {
                    point = (Point)point.Clone();
                    point.SetCoordinates(x, y);
                    if (IsOnCircuit(point))
                        list.Add(point);
                }
            }
            return list;
        }

        public List<Point> GetAllPointsWithoutCorners()
        {
            List<Point> list = new List<Point>();

            for (int x = start; x <= end; x++)
            {
                for (int y = start; y <= end; y++)
                {
                    point = (Point)point.Clone();
                    point.SetCoordinates(x, y);
                    if (IsOnCircuit(point) && !IsOnCorner(point))
                        list.Add(point);
                }
            }
            return list;
        }

        protected bool IsOnCircuit(Point point)
        {
            if (((point.X == start || point.X == end) && (point.Y >= start && point.Y <= end)) || 
                ((point.Y == start || point.Y == end) && (point.X >= start && point.X <= end)))
                return true;
            else
                return false;
        }

        protected bool IsOnCorner(Point point)
        {
            if ((point.X == start || point.X == end) && (point.Y == start || point.Y == end))
                return true;
            else
                return false;
        }

        public int Start { get => start; set => start = value; }
        public int End { get => end; set => end = value; }
    }
}
