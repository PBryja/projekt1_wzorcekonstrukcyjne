﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class Player
    {
        //private int lives;
        private Point currentPosition;
        private bool key = false;


        public Point CurrentPosition { get => currentPosition;}
        public bool Key { get => key; set => key = value; }


        //private Point previousPosition;

        public void SetPosition(Point point)
        {
            currentPosition = point;
        }

    }
}
