﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja

{
    class Program
    {
        static void Main(string[] args)
        {
            MazeBuilder builder = null;

            while (builder==null)
            {
                Console.WriteLine("Wybierz poziom trudnosci: Wpisz easy lub medium.");
                string read = Console.ReadLine();

                if (read == "easy")
                {
                    builder = new EasyMazeBuilder();
                }

                else if (read == "medium")
                {
                    builder = new MediumMazeBuilder();
                }
            }

            Point point = new Point(1, 1);
            SupervisorMaze supervisor = new SupervisorMaze();
            Maze maze = supervisor.ConstructMaze(builder);
            Movement movement = new Movement(maze);
            Player player = new Player();
            player.SetPosition(point);
            Circuit circuit = new Circuit(0, 0);
            IVisibility vis = new Visibility();

            bool finish = false;
            string command = "";
            string raport = "";
            while (!finish)
            {
                Console.Clear();

                Console.WriteLine(vis.Generate(player.CurrentPosition, maze));
                Console.WriteLine("Uzywaj strzalek zeby sie poruszac.\n\nLegenda:\n# - sciana\n  - pokoj (puste pole)\n+ - drzwi\n* - artefakt\nNacisnij t, aby wprowadzic komende");
                if (raport.Length > 0)
                {
                    Console.WriteLine(raport);
                }
                string name = Console.ReadKey().Key.ToString();

                if (name.ToString() == "T")
                {
                    Console.Clear();
                    Console.WriteLine("Lista komend: \nexit \ngod");
                    command = Console.ReadLine();

                }
                else
                    command = name;

                command = command.ToLower();

                
                if(command.Equals("uparrow") || command.Equals("downarrow") || command.Equals("leftarrow") || command.Equals("rightarrow"))
                {
                    point = movement.Move(player, command);

                    if (point != null)                 
                        player.SetPosition(point);


                }

                if (command.Equals("god"))
                {
                    vis = VisibilityFactoryMethod.FactoryMethod("god");
                }

                if (command.Equals("exit"))
                {
                    finish = true;
                    continue;
                }

                if (maze.Board[player.CurrentPosition.X,player.CurrentPosition.Y] is RoomEnd)
                {
                    Console.WriteLine("Brawo wygrales!");
                    Console.ReadLine();
                    finish = true;
                }

                if (maze.Board[player.CurrentPosition.X, player.CurrentPosition.Y] is RoomKey)
                {
                    raport = "Znalazles klucz! Teraz mozesz otworzyc drzwi.";
                    player.Key = true;
                    maze.SetField(player.CurrentPosition.X, player.CurrentPosition.Y, new Room());
                }
            }
        }
    }
}