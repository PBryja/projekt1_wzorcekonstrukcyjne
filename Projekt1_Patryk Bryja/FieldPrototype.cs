﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    abstract class FieldPrototype : ICloneable
    {
        public abstract object Clone();
    }
}
