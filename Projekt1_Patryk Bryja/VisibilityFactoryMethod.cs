﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Patryk_Bryja
{
    class VisibilityFactoryMethod
    {
        public static IVisibility FactoryMethod(string name)
        {
            switch (name)
            {
                case "god":
                    return new GodVisibility();
                case "normal":
                    return new Visibility();
                default:
                    return new Visibility();
            }
        }
    }
}
